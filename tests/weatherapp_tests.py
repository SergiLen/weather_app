import os
import weatherapp
import unittest
import tempfile
import json

class WeatherappTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, weatherapp.app.config['DATABASE'] = tempfile.mkstemp()
        weatherapp.app.testing = True
        self.app = weatherapp.app.test_client()
        with weatherapp.app.app_context():
            # print(dir(weatherapp.weatherapp),dir(weatherapp.app))
            weatherapp.weatherapp.setup()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(weatherapp.app.config['DATABASE'])

    def test_api_hello(self):
        rtt= self.app.get('/weather')
        assert b'Returns json file of weather data for the city acquired' in rtt.data

    def test_summary(self):
        rtt=self.app.get('/weather/london/20180315/1800')
        dat= json.loads(rtt.get_data(as_text=True))
        assert dat['description'] == "light rain" 
        assert dat['humidity'] == "82%" 
        assert dat['pressure'] == "997.3" 
        assert dat['temperature'] == "10C" 
    def test_no_data(self):
        rtt=self.app.get('/weather/london/17670812/0900/temperature')
        dat= json.loads(rtt.get_data(as_text=True))
        assert dat['status'] == 'error'
        assert dat['message'] == 'No data for 1767-08-12 09:00'


if __name__ == '__main__':
    unittest.main()