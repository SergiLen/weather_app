#!/usr/bin/env bash

apt-get update
apt-get install -y python-pip python-dev build-essential 
apt-get install -y build-essential autoconf libtool pkg-config python-opengl python-imaging python-pyrex python-pyside.qtopengl idle-python2.7 qt4-dev-tools qt4-designer libqtgui4 libqtcore4 libqt4-xml libqt4-test libqt4-script libqt4-network libqt4-dbus python-qt4 python-qt4-gl libgle3 python-dev libssl-dev
apt-get install libxml2-dev libxslt1-dev

apt-get install -y libfreetype6-dev libpng-dev

exit
# sudo -H pip install --upgrade pip
# sudo apt-get install python-matplotlib
# sudo -H pip install --upgrade matplotlib
# sudo -H pip install lxml
# gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
# \curl -sSL https://get.rvm.io | bash -s stable --ruby
# gem install koala
# gem install pry