# weatherapp

A small flask api in Python 3 for digest data from json. 

## Install

Use of flask `pip install flask`

## Running
### To run the application 
* change directory into weatherapp and run `export FLASK_APP=weatherapp.py` 
* `run flask` run on console
* navigate to http://127.0.0.1:5000/ 
api endpoints 
* /weather hello endpoint with some help
* /weather/city/date/hourminute for a quick summary for the city, date, and time
* /weather/city/date/hourminute/info for the selected info



#### The application is tested to work in Ubuntu 16.04.

#### Vagrant support through 
`Vagrant init`, `Vagrant up`

#### Kelvin or Celcius in branch `celsius_or_kelvin` 
to use the new feature just add ?degree=degree, where degree is Kelvin or Celcius.

#### coverage and testing in the CI/CD 

#### to run tests 
after install `pip install -e . ` python tests/weatherapp_tests.py