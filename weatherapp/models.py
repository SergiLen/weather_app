from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base

# class City(Base):
#     __tablename__ = 'city'

#     id = Column(Integer, primary_key=True)
#     name = Column(String)
#     lat = Column(String)
#     lon = Column(String)
#     country = Column(String)
#     population = Column(Integer)

#     forecasts = relationship("Forecast", backref="city", order_by="Forecast.id")
Base = declarative_base()
class Forecast(Base):
    __tablename__ = 'forecasts'

    id = Column(Integer, primary_key=True)
    # city_id = Column(Integer, ForeignKey('city.id'))
    city_name = Column(String,index= True)
    description = Column(String)
    temperature = Column(String)
    pressure = Column(String)
    humidity = Column(Integer)
    date = Column(DateTime, index= True)

