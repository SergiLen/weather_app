from flask import Flask,json, Response,jsonify,request
from weatherapp.models import Forecast, Base
from flask_sqlalchemy import SQLAlchemy
import datetime
import os
from sqlalchemy import func


app = Flask(__name__)
app.config.from_object(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
db = SQLAlchemy(app)

@app.before_first_request
def setup():
    Base.metadata.drop_all(bind=db.engine)
    Base.metadata.create_all(bind=db.engine)
    with open(os.path.join('weatherapp','data','forecast.json')) as fop:
        dat= json.load(fop)

        cit=dat['city']
        for forecast in dat['list']:
            forc = Forecast(id = forecast['dt'],
                city_name = cit['name'].lower(),
                # city_id = cit['id'], 
                description = forecast['weather'][0]['description'],
                temperature = forecast['main']['temp'],
                pressure = forecast['main']['pressure'],
                humidity = forecast['main']['humidity'],
                date = datetime.datetime.strptime(forecast['dt_txt'] , '%Y-%m-%d %H:%M:%S'))#.strftime('%Y-%m-%d %H:%M')
            db.session.add(forc)
        db.session.commit()

@app.route('/weather')
def hello_from_api():
    return 'Returns json file of weather data for the city acquired. Use /date/hourminute/ \
    for summary of this date and time \
    /<option> with option in the list [temperature on Celsius scale, pressure, humidity ]'

@app.route('/weather/<city>/<date>/<hourminute>',methods =['GET'])
def api_weather_summary(city,date,hourminute):
    query = db.session.query(Forecast).filter((Forecast.city_name == city.lower()) & (Forecast.date == datetime.datetime.strptime(date + ' '+hourminute[:2]+':'+hourminute[2:], '%Y%m%d %H:%M'))).one_or_none()
    if query !=None:

        retw= {'description':query.description,'temperature':str(round(float(query.temperature)- 273.15))+'C','pressure':query.pressure, 'humidity': str(query.humidity)+'%'}
        return jsonify(retw)
    else:
        return not_found()


@app.route('/weather/<city>/<date>/<hourminute>/<info>',methods =['GET'])
def api_weather_info(city,date,hourminute,info):
    query = db.session.query(Forecast).filter((Forecast.city_name == city.lower()) & (Forecast.date == datetime.datetime.strptime(date + ' '+hourminute[:2]+':'+hourminute[2:], '%Y%m%d %H:%M'))).one_or_none()
    if query !=None:
        retw= {'description':query.description,'temperature':str(round(float(query.temperature)- 273.15))+'C','pressure':query.pressure, 'humidity': str(query.humidity)+'%'}
        if info in retw:
            return jsonify({info:retw[info]})
        else:
            return not_found()
    else:
        return not_found()


@app.errorhandler(404)
def not_found(error= None):
    rr = request.view_args

    message = {
            'status': "error",
            'message': "No data for " + datetime.datetime.strptime(rr['date'] + ' '+rr['hourminute'][:2]+':'+rr['hourminute'][2:], '%Y%m%d %H:%M').strftime('%Y-%m-%d %H:%M'),
    }
    resp= jsonify(message)
    resp.status_code = 404
    return resp

if __name__ == '__main__':
    app.run()
    app.setup()