try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='weatherapp',
    version='0.1',
    description='Load data from csv and amazon S3 and creates a flask API for product retrieval',
    author='Sergios Lenis',
    author_email='sergioslenis@gmail.com',
    license='MIT',
    packages=['weatherapp'],
    include_package_data=True,
    data_files = [('weatherapp/data', ['weatherapp/data/*.json']),],
    # package_data={'weatherapp': ['weatherapp/data/*.json']},
    install_requires=[ 'flask', 'flask_sqlalchemy'],
        )